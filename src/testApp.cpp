#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){

    arduino.listDevices();
    arduino.setup(0, 9600);
    arduino.flush(true,true);
    serialRXStatus = RX_READY;
    serialTXStatus = TX_READY;

}

//--------------------------------------------------------------
void testApp::update(){

    serialRX();

}

//--------------------------------------------------------------
void testApp::draw(){

}


//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){
    if(button == 0){
    serialTX();
    }
    if(button == 2){
    serialTXLight(x);
    }
}


void testApp::serialRX() {

    while (arduino.available() > 0){

        bufferCharRX = arduino.readByte();

        switch (serialRXStatus){

            case RX_READY://---------------------------------------------------------------------------------

                switch(bufferCharRX){
                    case 1:
                        cout << "START\n";
                        serialRXStatus = RX_COMMAND;
                    break;
                    case 6:
                        serialTXStatus = TX_READY;
                        cout << "serial ready\n";
                    break;
                    default:
                        cout << "RECEIVE: error\n";
                    break;
                }
            break;

            case RX_COMMAND://---------------------------------------------------------------------------------

                switch (bufferCharRX){
                    case 'A':
                        serialCommand = POSITION;
                        serialRXStatus = RX_DATA;
                    break;

                    case 'B':
                        serialCommand = MISC;
                        serialRXStatus = RX_DATA;
                    break;
                    default:
                        cout << "COMMAND: error\n";
                    break;
                    }

            break;

            case RX_DATA://-------------------------------------------------------------------------------

                switch (serialCommand){

                    case POSITION:

                        if(bufferCharRX != 4){
                            bufferRX[bufferIndexRX] = bufferCharRX;
                            bufferIndexRX++;
                            cout << "Command\n";
                        } else {

                            //Include terminating NULL in buffer end - strtok looks for it to stop
                            bufferRX[bufferIndexRX] = '\0';
                            //StrTok delimit buffer
                            strPointerRX = strtok(bufferRX, delimiterRX);
                            for(int i=0; i < 2; i++){ //Go sthrough 2 sections - command,command
                                commandRX[i] = strPointerRX;
                                strPointerRX = strtok(NULL, delimiterRX);
                            }

                            cout << "END: "+ofToString(commandRX[0])+ofToString(commandRX[1])+"\n";

                            bufferIndexRX=0;
                            serialRXReady();
                        }

                    break;

                    case MISC:

                        cout << "command B";

                    break;
                    default:
                        cout << "ERROR\n";
                    break;
                }
            break;


        }

    }
  }

void testApp::serialRXReady(){
    arduino.writeByte(6);
    serialRXStatus = RX_READY;
}

void testApp::serialTX(){
    if(serialTXStatus==TX_READY){
        //Create string to send
        commandStrSizeTX = sprintf (commandTX, "%c%c%i,%i%c", 1, 'A',888,999,4);
        cout << "Buffer sent: "+ofToString(commandTX)+"\n";
        //Send buffer
        arduino.writeBytes((unsigned char*)commandTX, commandStrSizeTX);
        serialTXStatus = TX_WAITING;
    }
}



 void testApp::serialTXLight(int delay){
    if(serialTXStatus==TX_READY){
        //Create string to send
        commandStrSizeTX = sprintf (commandTX, "%c%c%i,%i%c", 1, 'B', 13, delay, 4);
        cout << "Buffer sent: "+ofToString(commandTX)+"\n";
        //Send buffer
        arduino.writeBytes((unsigned char*)commandTX, commandStrSizeTX);
        serialTXStatus = TX_WAITING;

}
 }
