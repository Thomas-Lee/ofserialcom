#pragma once

#include "ofMain.h"

class testApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void mouseReleased(int x, int y, int button);

		void serialRX();
		void serialTX();
		void serialTXLight(int delay);

		void serialRXReady();

        //Serial object
		ofSerial arduino;

		//TX
		char commandTX[40];
		int commandStrSizeTX;


		//RX
		char bufferRX[40], bufferCharRX;
		int bufferSizeRX, bufferIndexRX;

        //RX status
		typedef enum e_serialRXStatus{RX_READY,RX_COMMAND,RX_DATA,};
		e_serialRXStatus serialRXStatus;

		typedef enum e_serialTXStatus{TX_READY,TX_WAITING};
		e_serialTXStatus serialTXStatus;


        //RX command
		enum e_serialCommand{POSITION,MISC};
		e_serialCommand serialCommand;

        //RX - strtok
        const char delimiterRX[1] = {','};
        const char* commandRX[2]; //Size of command
        char* strPointerRX; //pointer to string start

};
